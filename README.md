This is a basic test that shows iot traffic via ueransim and open5gs.

Run the following commands before starting docker-compose:

mkdir -p ~/.mytb-data && sudo chown -R 799:799 ~/.mytb-data

mkdir -p ~/.mytb-logs && sudo chown -R 799:799 ~/.mytb-logs

Refer to the thingsboard documentation for more info about running the IOT
